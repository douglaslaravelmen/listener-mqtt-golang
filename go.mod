module gitlab.com/contatodouglasmen/listener-mqtt-golang

go 1.14

require (
	github.com/emitter-io/go v1.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.3.5
)

package main

import (
	"encoding/json"
	"log"
	"github.com/streadway/amqp"
	"strings"
	"fmt"
	"context"
	"os"
	"go.mongodb.org/mongo-driver/bson"
    "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"github.com/joho/godotenv"	
)

func main() {

	// Abre Listener
	msgs, err := consumeQueue()

	// Handler do Erro de Abertura do Listener
	failOnError(err, "Falha para registrar o listener")

	// Abrindo Conexão com o MongoDB
	client := connectMongoDB()

	// Selecionando Banco e Collection no MongoDB	
	collection := getMongoDBCollection("zordon", "mensagens", client)

	// Declara Loop
	forever := make(chan bool)
	
	// Processa Mensagens Recebidas 
	go processMessage(msgs, *collection)

	// Mensagem Para Informar Usuário
	log.Printf(" [*] Aguardando mensagens. Para sair aperte CTRL+C")

	// Invocando Loop
	<-forever 
}

// Mensagem representa o conteúdo de um payload recebido via mqtt
type Mensagem struct {    
	Active   	 	int64 `bson:"active" json:"active"`
	Conn 		 	int64 `bson:"conn" json:"conn"`
	PBatt 		 	int64 `bson:"pbatt" json:"pbatt"`
	PfType 		 	int64 `bson:"pf_type" json:"pf_type"`
	PowerFactor  	int64 `bson:"power_factor" json:"power_factor"`
	Reactive     	int64 `bson:"reactive" json:"reactive"`
	RepoActive   	int64 `bson:"repo_active" json:"repo_active"`
	RepoReactive 	int64 `bson:"repo_reactive" json:"repo_reactive"`
	SigLvl    	 	int64 `bson:"siglvl" json:"siglvl"`
	SpiffsJSONCount int64 `bson:"spiffs_json_count" json:"spiffs_json_count"`
	SpiffsMmCount 	int64 `bson:"spiffs_mm_count" json:"spiffs_mm_count"`
	Status 		 	int64 `bson:"status" json:"status"`
	SysHeap 		int64 `bson:"sys_heap" json:"sys_heap"`
	Time 		 	int64 `bson:"time" json:"time"`
	Vbatt    	 	int64 `bson:"vbatt" json:"vbatt"`
	Version 		int64 `bson:"version" json:"version"`
	VoltaAlarm   	bool  `bson:"voltaalarm" json:"voltaalarm"`
	Voltage 		int64 `bson:"voltage" json:"voltage"`
}

// env Encontra e retorna o valor de uma variável de ambiente
func env(key string) string {	
	err := godotenv.Load(".env")
	failOnError(err, "Erro ao Carregar Dados de Ambiente")
	return os.Getenv(key)
}

// brokerDial Conecta no Broker
func brokerDial() (*amqp.Connection, error) {
	return amqp.Dial(
		"amqp://" + 
		env("AMQP_USER") + 
		":" + 
		env("AMQP_PASSWORD") + 
		"@" + 
		env("AMQP_HOSTNAME") + 
		":" + 
		env("AMQP_PORT"))
}

// channelDial Abre um canal na conexão com o Broker
func channelDial(conn *amqp.Connection) (*amqp.Channel, error) {
	return conn.Channel()
}

// consumeQueue Abre Listener
func consumeQueue() (<-chan amqp.Delivery, error) {

	// Tentativa Conexão com Broker
	conn, err := brokerDial()

	// Handler do Erro de Conexão com Broker
	failOnError(err, "Falha ao conectar ao RabbitMQ")

	// Abre Canal de Comunicação com Broker	
	ch, err := channelDial(conn)
	
	// Handler do Erro de Abertura do Canal
	failOnError(err, "Falha ao abrir o canal")

	return ch.Consume("zordon", "", true,	false, false, false, nil)
}

// connectMongoDB Conecta no banco MongoDB
func connectMongoDB() (*mongo.Client) {
	// Setando opções do Cliente MongoDB	
	clientOptions := options.Client().ApplyURI("mongodb://" + env("MONGO_HOSTNAME") + ":" + env("MONGO_PORT"))	

	// Abrindo Conexão com o MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	// Handler do Erro de Abertura de Conexão com o Mongo
	failOnError(err, "Falha para conectar com o MongoDB")
	
	// Verifica Conexão com o MongoDB
	err = client.Ping(context.TODO(), nil)

	// Handler do Erro de Verificação de Conexão com o Mongo
	failOnError(err, "Falha para verificar o MongoDB")

	return client;
}

// getMongoDBCollection Retorna uma Collection do MongoDB
func getMongoDBCollection(database string, collection string, client *mongo.Client) (*mongo.Collection) {
	return client.Database("zordon").Collection("mensagens")
}

// processMessage Processa as Mensagens que Chegam no Broker
func processMessage(msgs <-chan amqp.Delivery, collection mongo.Collection) {

	i := 0

	// Loop nas mensagens recebidas
	for d := range msgs {

		// Dividindo String do Tópico
		topicSplitString := strings.Split(d.RoutingKey, ".")

		// Código da Probe
		probeCode := topicSplitString[1];

		// Estrutura Vazia
		messageData := Mensagem{}

		// Preenchendo Estrutura com o Conteúdo da Mensagem Recebida
		if err := json.Unmarshal(d.Body, &messageData); err != nil {
			panic(err)
		}

		// Inserindo Estrutura na Collection do MongoDB
		insertResult, err := collection.InsertOne(context.TODO(), bson.M{
			"codigo": probeCode,
			"time": messageData.Time,
			"active": messageData.Active,
			"power_factor": messageData.PowerFactor,
			"reactive": messageData.Reactive,
			"repo_active": messageData.RepoActive,
			"siglvl": messageData.SigLvl,
			"vbatt": messageData.Vbatt,
			"voltaalarm": messageData.VoltaAlarm,
			"read": false})

		// Handler do Erro de Inserção no MongoDB	
		failOnError(err, "Falha ao Registrar no MongoDB")

		// Informando o Usuário que a Mensagem foi Salva no MongoDB
		fmt.Println("Mensagem Registrada: ", insertResult.InsertedID)
		i++
	}

	fmt.Printf("%d Registros criados", i)
}

// failOnError Processa os Possíveis Erros de Processo
func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}